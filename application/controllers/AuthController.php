<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require(APPPATH . 'libraries/REST_Controller.php');
require(APPPATH . 'libraries/Format.php');

class AuthController extends REST_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('AuthModel');
    }

    function getUserAll_get(){
        $result = $this->AuthModel->getUserAll();
        $data = $result->result();
        $this->response(array('status' => 200, 'data' => $data));
    }

    function login_get(){
        $data = array(
            'username' => 'username',
            'password' => '123'
        );
        $this->response(array('status' => 'sucess', 'data' => $data));
    }
}